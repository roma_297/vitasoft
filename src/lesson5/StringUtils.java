package lesson5;

public class StringUtils {

    private static final String REGEX = "";

    public String method1(String string) {
        return string.replaceAll(REGEX, "");
    }

    public String method2(String string) {
        return helperMethod(string);
    }

    private String helperMethod(String string) {
        return string;
    }

    public String method3(String string) {
        return null;
    }

    public String method4(String string) {
        return null;
    }
}
