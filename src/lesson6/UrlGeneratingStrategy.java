package lesson6;

public interface UrlGeneratingStrategy {
    String generateUrl(String url);
}
