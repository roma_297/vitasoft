package lesson6;

import java.util.Arrays;

public class Main {
    public static final boolean flag = true;
    private static UrlGeneratingStrategy GENERATING_STRATEGY;

    static {
        GENERATING_STRATEGY = flag ? new DatabaseUrlGeneratingStrategy() : new RegexUrlGeneratingStrategy();
    }

    public static void main(String[] args) {
        Arrays.stream(args).forEach(arg -> System.out.println(GENERATING_STRATEGY.generateUrl(arg)));
    }
}
