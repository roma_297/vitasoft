package lesson6;

import java.math.BigDecimal;

public class ProblemExample {
    public static void main(String[] args) {
        Double doubleValue = StaticGenericMethod.<Double>method();
        Integer integerValue = StaticGenericMethod.<Integer>method();
    }
}

class Touple<A extends Number, B extends Number> {
    private A object1;
    private B object2;

    public Touple(A object1, B object2) {
        this.object1 = object1;
        this.object2 = object2;
    }

    public A getObject1() {
        return object1;
    }

    public B getObject2() {
        return object2;
    }

    public void setObject1(A object1) {
        this.object1 = object1;
    }

    public void setObject2(B object2) {
        this.object2 = object2;
    }
}

class StaticGenericMethod {
    public static <T extends Number> T method() {
        return (T) (new BigDecimal(123).add(new BigDecimal(100)));
    }
}