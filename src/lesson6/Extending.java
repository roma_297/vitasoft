package lesson6;

import lesson5.Person;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Extending {
    public static void main(String[] args) {
        Monster monster = new Monster();

        List<Person> persons = Arrays.asList(new Person("John"), new Person("Anna"));

        persons.stream()
                .filter(person -> !monster.scare(person))
                .filter(person -> !monster.kill(person))
                .forEach(person -> System.out.println(person + " survived!"));
    }
}

interface Scary {
    boolean scare(Person person);
}

interface Dangerous extends Scary {
    boolean kill(Person person);
}

class Monster implements Dangerous {

    private static final Random RANDOM = new Random();

    @Override
    public boolean scare(Person person) {
        boolean wasScared = RANDOM.nextBoolean();
        System.out.println(person + " was" + (wasScared ? "" : " not") + " scared");
        return wasScared;
    }

    @Override
    public boolean kill(Person person) {
        boolean wasKilled = RANDOM.nextBoolean();
        System.out.println(person + " was" + (wasKilled ? "" : " not") + " killed");
        return wasKilled;
    }
}
