package lesson7.inner;

public class Main {
    public static void main(String[] args) {
        SomeJob someJob = new SomeJob();
        SomeJob.TransactionWithoutResult transactionWithoutResult1 = someJob.new TransactionWithoutResult();
        SomeJob.TransactionWithoutResult transactionWithoutResult2 = someJob.new TransactionWithoutResult();
    }
}
