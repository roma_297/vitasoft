package lesson7.inner;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class SomeJob {
    private StockService stockService = new StockService();
    private MailService mailService = new MailService();
    private List<Notification> notifications = Collections.emptyList();

    public void perform() {
        TransactionWithoutResult transactionWithoutResult = new TransactionWithoutResult();
        transactionWithoutResult.performInTransaction();
    }

    public class TransactionWithoutResult {
        public void performInTransaction() {
            for (Notification notification: notifications) {
                if (stockService.isProductInStock(notification.getGood().getId())) {
                   mailService.sendEmail(notification.getPerson().getEmail());
                }
            }
        }
    }
}
