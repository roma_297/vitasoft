package lesson7.inner;

public class Notification {
    private Person person;
    private Good good;

    public Notification(Person person, Good good) {
        this.person = person;
        this.good = good;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }
}
