package lesson7;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class CollectionExample {
    public static void main(String[] args) {
        Set<Integer> numbers = new HashSet<>(Arrays.asList(1, 2, 3, 5, 213, 43, 239, 4322));

        Iterator<Integer> iterator = numbers.iterator();

        while (iterator.hasNext()) {
            Integer next = iterator.next();

            if (isEven(next)) {
                iterator.remove();
            }
        }

        numbers.forEach(System.out::println);
    }

    private static boolean isEven(int value) {
        return value % 2 == 0;
    }
}
