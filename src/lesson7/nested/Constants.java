package lesson7.nested;

public class Constants {

    public static class Urls {

        public static String URL1 = "";
        public static String URL2 = "";

        private Urls() {
        }
    }

    public static class ErrorMessages {
        public static String ERROR_MESSAGE_1 = "";
        public static String ERROR_MESSAGE_2 = "";

        private ErrorMessages() {
        }
    }

    public static class ConfigurationParameterKeys {
        public static String CONFIG_KEY_1 = "";
        public static String CONFIG_KEY_2 = "";

        private ConfigurationParameterKeys() {
        }
    }

    private Constants() {
    }
}
