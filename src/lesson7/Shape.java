package lesson7;

public abstract class Shape implements Squarable {
    protected String description;

    public void printFigureDescription() {
        System.out.println(description);
    }

//    public abstract double getSquare();
}
