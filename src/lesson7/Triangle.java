package lesson7;

import java.util.Random;

public class Triangle extends Polynom {
    private static final Random RANDOM = new Random();

    private Point point1;
    private Point point2;
    private Point point3;

    public Triangle(Point point1, Point point2, Point point3) {
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
    }

    @Override
    public double getSquare() {
        return RANDOM.nextDouble();
    }
}
