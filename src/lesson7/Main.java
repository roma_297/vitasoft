package lesson7;

public class Main {
    public static void main(String[] args) {
        Point point = new Point(0, 0);
        Shape shape = new Circle(point, 1);
        System.out.println(shape.getSquare());
    }
}
