package lesson7;

public class Rectangle extends Polynom {
    protected double height;
    protected double width;

    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public double getSquare() {
        return height * width;
    }
}
