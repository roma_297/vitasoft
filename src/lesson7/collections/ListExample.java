package lesson7.collections;

import java.util.LinkedList;
import java.util.Queue;

public class ListExample {
    public static void main(String[] args) {
        Queue<String> strings = new LinkedList<>();
        strings.add("string1");
        strings.add("string2");
        strings.add("string3");

        System.out.println("Head: " + strings.peek());
        strings.forEach(System.out::println);
    }
}
