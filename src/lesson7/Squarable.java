package lesson7;

public interface Squarable {
    double getSquare();
}