package lesson9.maps;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class MapDemo {
    public static void main(String[] args) {
//        HashMap<String, String> countriesCapitalsMap = new HashMap<>();
//
//        countriesCapitalsMap.put("Germany", "Berlin");
//        countriesCapitalsMap.put("Russia", "Moscow");
//        countriesCapitalsMap.put("France", "Paris");
//
//        Iterator<String> iterator = countriesCapitalsMap.keySet().iterator();
//        while (iterator.hasNext()) {
//            String key = iterator.next();
//            if (key.contains("many")) {
//                iterator.remove();
//            }
//        }
//
//        for (Map.Entry<String, String> countryCapitalEntry : countriesCapitalsMap.entrySet()) {
//            System.out.println(countryCapitalEntry.getValue() + " is a capital of " + countryCapitalEntry.getKey());
//        }

        LinkedHashMap<String, Country> countries = new LinkedHashMap<>();
        countries.put("Russia", new Country("Russia", "Moscow", 150_000_000));
        countries.put("Germany", new Country("Germany", "Berlin", 100_000_000));

        countries.forEach((key, value) -> System.out.println(value));
    }
}

class Country {
    String name;
    String capital;
    int population;

    public Country(String name, String capital, int population) {
        this.name = name;
        this.capital = capital;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", population=" + population +
                '}';
    }
}

