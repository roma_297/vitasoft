package lesson9.iterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class IteratorDemo {


    public static void main(String[] args) {
//        List<String> strings = new ArrayList<>(Arrays.asList("", "sd", "sdsd", "aaasd", "1231"));
//
//        Iterator<String> iterator = strings.iterator();
//
//        while (iterator.hasNext()) {
//            String string = iterator.next();
//            if (string.contains("sd")) {
//                iterator.remove();
//            }
//        }
//
//        strings.forEach(System.out::println);


        LinkedList<String> strings1 = new LinkedList<>(Arrays.asList("", "sd", "sdsd", "aaasd", "1231"));
        Iterator<String> descendingIterator = strings1.descendingIterator();
        while (descendingIterator.hasNext()) {
            String string = descendingIterator.next();
            System.out.println(string);
        }
        String next = descendingIterator.next();

    }
}
