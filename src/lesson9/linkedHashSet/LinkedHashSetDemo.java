package lesson9.linkedHashSet;

import java.util.LinkedHashSet;

public class LinkedHashSetDemo {
    public static void main(String[] args) {
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();

        linkedHashSet.add("");
        linkedHashSet.add("a");
        linkedHashSet.add("sdf");

        linkedHashSet.forEach(System.out::println);

    }
}
