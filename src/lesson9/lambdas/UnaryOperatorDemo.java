package lesson9.lambdas;

import java.util.function.UnaryOperator;

public class UnaryOperatorDemo {
    public static void main(String[] args) {
        Performer performer = new Performer();

        performer.perform((Integer a) -> a + 1, 3);
        performer.perform((Integer a) -> a * 2, 123);
        performer.perform((Integer a) -> a / 2, 239);
    }
}

class Performer {
    void perform (UnaryOperator<Integer> operator, int a) {
        int result = operator.apply(a);
        System.out.println("Operator applied. Result: " + result);
    }
}