package lesson9.lambdas;

public class BinaryOperationDemo {
    public static void main(String[] args) {
        BinaryOperationPerformer binaryOperationPerformer = new BinaryOperationPerformer();

        binaryOperationPerformer.perform((int a, int b) -> a + b, 4, 234);
        binaryOperationPerformer.perform((int a, int b) -> a - b, 4, 234);
        binaryOperationPerformer.perform((int a, int b) -> a * b, 4, 234);
        binaryOperationPerformer.perform((int a, int b) -> a / b, 300, 32);
    }
}

@FunctionalInterface
interface BinaryOperation {
    int perform(int a, int b);
}

class BinaryOperationPerformer {
    void perform(BinaryOperation binaryOperation, int a, int b) {
        int result = binaryOperation.perform(a, b);
        System.out.println("Operation performed. Result: " + result);
    }
}


