package lesson9.lambdas;

import java.util.Random;

public class GreetingDemo {
    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        Greeting greetInformally = () -> { System.out.println("Hi!"); };
        new Greeter().greet(greetInformally);
    }
}

@FunctionalInterface
interface Greeting {
    void greet();
}

class Greeter {
    void greet(Greeting greeting) {
        greeting.greet();
    }
}