package lesson9.lambdas;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

public class LambdaDemo {

    private static final List<Product> PRODUCTS = Arrays.asList(
            new Product(0, 143, Size.S),
            new Product(1, 123, Size.XL),
            new Product(3, 645, Size.M),
            new Product(5, 645, Size.M),
            new Product(6, 645, Size.M),
            new Product(4, 45, Size.S),
            new Product(7, 645, Size.L),
            new Product(2, 645, Size.S),
            new Product(12, 645, Size.XL));
    
    public static void main(String[] args) {
        TreeSet<Product> products = new TreeSet<>((Product o1, Product o2) -> o1.getSize() == o2.getSize() ? (o1.getColorCode() - o2.getColorCode()) : (o1.getSize().ordinal() - o2.getSize().ordinal()));

        products.addAll(PRODUCTS);

        products.forEach(System.out::println);
    }
}


class Product {
    private int id;
    private int colorCode;
    private int colorDescription;
    private Size size;

    public Product(int id, int colorCode, Size size) {
        this.id = id;
        this.colorCode = colorCode;
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getColorCode() {
        return colorCode;
    }

    public void setColorCode(int colorCode) {
        this.colorCode = colorCode;
    }

    public int getColorDescription() {
        return colorDescription;
    }

    public void setColorDescription(int colorDescription) {
        this.colorDescription = colorDescription;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "ProductModel{" +
                "id=" + id +
                ", colorCode=" + colorCode +
                ", size=" + size +
                '}';
    }
}

enum Size {
    XS, S, M, L, XL, XXL
}
