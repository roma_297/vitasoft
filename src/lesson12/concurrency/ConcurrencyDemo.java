package lesson12.concurrency;

public class ConcurrencyDemo {
    public static void main(String[] args) throws InterruptedException {
        TalkThread talkThread = new TalkThread();
        Thread walkThread = new Thread(new WalkThread());


        System.out.println(Thread.currentThread().getName());
        System.out.println(Thread.currentThread().getId());

        talkThread.start();
        walkThread.start();
    }
}

class TalkThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Talking");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
        }
    }
}

class WalkThread implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Walking");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
        }
    }
}