package lesson12.concurrency;

public class DaemonDemo {
    public static void main(String[] args) {
        DaemonThread daemonThread = new DaemonThread();
//        daemonThread.setDaemon(true);
        daemonThread.start();
    }
}

class DaemonThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            System.out.println(i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
