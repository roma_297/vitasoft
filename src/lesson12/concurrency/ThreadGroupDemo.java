package lesson12.concurrency;

public class ThreadGroupDemo {
    public static void main(String[] args) throws InterruptedException {
        ThreadGroup group = new ThreadGroup("Group");
        new NodeThread(group, "thread1").start();
        new NodeThread(group, "thread2").start();
        new NodeThread(group, "thread3").start();
        new NodeThread(group, "thread4").start();

        group.list();

        Thread.sleep(1000);

        group.interrupt();

        System.out.println(group.activeCount());
    }
}

class NodeThread extends Thread {

    public NodeThread(ThreadGroup group, String name) {
        super(group, name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(getName() + ": i = " + i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
        }
    }
}
