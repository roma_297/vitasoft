package lesson12.concurrency;

import java.math.BigDecimal;

public class PlannerDemo {
    public static void main(String[] args) {
        MyThread thread1 = new MyThread();
        thread1.setPriority(Thread.MAX_PRIORITY);
        MyThread thread2 = new MyThread();
        thread2.setPriority(Thread.MIN_PRIORITY);

        thread1.start();
        thread2.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(thread1.getCounterStringRepresentation());
        System.out.println(thread2.getCounterStringRepresentation());

        thread1.interrupt();
        thread2.interrupt();
    }
}

class MyThread extends Thread {
    private static final Object monitor = new Object();

    private BigDecimal counter = new BigDecimal(0);

    public String getCounterStringRepresentation() {
        return counter.toString();
    }

    @Override
    public void run() {
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            synchronized (monitor) {
                counter = counter.add(new BigDecimal(1));
            }
        }
    }
}