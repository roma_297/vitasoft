package lesson12;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Demo {
    public static void main(String[] args) {
        ArrayList objects = new ArrayList<>();
        objects.add("a");
        objects.add("b");
        objects.add("c");
        objects.add("a");

        if (objects.remove("a")) {
            if (objects.remove("a")) {
                objects.remove("b");
            } else {
                objects.remove("c");
            }
        }

        System.out.println(objects);
    }
}

class MyClass {

    public static void main(String[] args) {
        System.out.println((String) new Object());
    }
}

abstract class AbstractClass {
    public abstract void doSomething();
}
