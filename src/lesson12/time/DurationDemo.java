package lesson12.time;

import java.time.Duration;
import java.util.Date;

public class DurationDemo {
    public static void main(String[] args) {
        Duration duration = Duration.ofSeconds(123);
        duration = duration.plusSeconds(7);
        System.out.println(duration.getSeconds());

        duration = duration.minusSeconds(10);
        System.out.println(duration.getSeconds());
        System.out.println(duration.toMillis());
        System.out.println(duration.multipliedBy(2).getSeconds());
    }
}
