package lesson12.time;

import sun.jvm.hotspot.debugger.win32.coff.DumpExports;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

public class DateTimeOperationsDemo {
    public static void main(String[] args) {
        LocalDate date1 = LocalDate.parse("01/12/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        LocalDate date2 = LocalDate.parse("29/12/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        Period period = Period.between(date1, date2);
//        System.out.println("Duration between two dates equals " + period.getDays());

        LocalTime time1 = LocalTime.parse("12:30:23");
        LocalTime time2 = LocalTime.parse("12:40:55");
        Duration duration = Duration.between(time1, time2);
//        System.out.println("Duration between two moments of time equals " + duration.getSeconds());

        Instant start = Instant.now();
        try {
            Thread.sleep(239);
        } catch (InterruptedException e) {

        }
        Instant end = Instant.now();
        Duration between = Duration.between(start, end);
        System.out.println(between.getNano());
    }
}
