package lesson12.time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

public class JavaDateAndTimeApiDemo {
    public static void main(String[] args) {
        LocalDate currentDate = LocalDate.now();
        LocalDate someDate = LocalDate.of(1990, 2, 5);
        LocalDate someDate1 = LocalDate.of(1990, Month.FEBRUARY, 5);

//        System.out.println(currentDate);
//        System.out.println(someDate);
//        System.out.println(someDate1);

        LocalTime currentTime = LocalTime.now();
        LocalTime someTime = LocalTime.of(12, 30);

//        System.out.println(currentTime);
//        System.out.println(someTime);

        LocalDateTime currentDateTime = LocalDateTime.now();
        LocalDateTime currentDateTime1 = LocalDateTime.of(currentDate, currentTime);

//        System.out.println(currentDateTime);
//        System.out.println(currentDateTime1);

        LocalDate parsedDate = LocalDate.parse("2018-11-29");
        LocalDate parsedDate1 = LocalDate.parse("29.11.2018", DateTimeFormatter.ofPattern("dd.MM.yyyy"));

        parsedDate1.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        System.out.println(parsedDate1.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
    }
}
