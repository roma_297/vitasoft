package lesson10.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectStreamDemo {
    public static void main(String[] args) {
        String min = Stream.of("My, name", "City", "bright, color", "happy")
                .parallel()
                .flatMap(str -> Arrays.stream(str.split(",")))
                .map(String::trim)
                .map(String::toLowerCase)
                .max(((o1, o2) -> o1.length() == o2.length() ? o1.compareTo(o2) : o1.length() - o2.length())).orElse("");

        System.out.println(min);
    }
}
