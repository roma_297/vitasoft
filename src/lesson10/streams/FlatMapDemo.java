package lesson10.streams;

import java.util.Arrays;
import java.util.stream.Stream;

public class FlatMapDemo {
    public static void main(String[] args) {
        String s = Stream.of("My, name", "City", "bright, color", "happy")
                .flatMap(str -> Arrays.stream(str.split(",")))
                .map(String::trim)
                .filter("name1"::equals)
                .findAny().orElse("");

        System.out.println(s + ": " + s.length());
    }
}
