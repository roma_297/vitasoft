package lesson10.streams;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DistinctStreamDemo {
    public static void main(String[] args) {
        List<String> distinctStrings = Stream.of("str1", "str2", "str1", "str1", "str2", "str3", "str1", "str4")
                .distinct().collect(Collectors.toList());

        distinctStrings.forEach(System.out::println);

    }
}
