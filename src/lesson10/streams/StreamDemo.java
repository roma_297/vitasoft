package lesson10.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamDemo {

    private static final List<ProductModel> PRODUCT_MODELS = Arrays.asList(
            new ProductModel(32, "Мыло", true),
            new ProductModel(0, "Гель", true),
            new ProductModel(2, "Мочалка", true),
            new ProductModel(5, "Веревка", false),
            new ProductModel(4, "Шампунь", true),
            new ProductModel(7, "Табуретка", true),
            new ProductModel(11, "Петля", false)
    );

    public static void main(String[] args) {

        List<ProductData> productDatas = PRODUCT_MODELS.stream()
                .filter(ProductModel::isInStock)
                .skip(1)
                .map(productModel -> new ProductData(productModel.getId(), productModel.getName()))
                .sorted(Comparator.comparingInt(ProductData::getId))
                .peek(System.out::println)
                .collect(Collectors.toList());

// Способы создания стримов
        ArrayList<ProductModel> productModels = new ArrayList<>(PRODUCT_MODELS);
        Stream<ProductModel> stream = productModels.stream();

        Stream<ProductModel> soapStream1 = Stream.of(new ProductModel(14, "Жидкое мыло", false));
        Stream<ProductModel> soapStream2 = Stream.of(new ProductModel(14, "Жидкое мыло", false), new ProductModel(15, "", true));

        ProductModel[] productModelsArray = new ProductModel[10];
        Stream<ProductModel> streamOnArray1 = Arrays.stream(productModelsArray);
        Stream<ProductModel> streamOnArray2 = Arrays.stream(productModelsArray, 2, 5);

        IntStream chars = "SomeString".chars();

        Stream<Object> stream4 = Stream.builder()
                .add(new ProductModel(1, "", false))
                .add(new ProductModel(2, "", true))
                .add(new ProductModel(3, "", false))
                .build();

        Stream<ProductModel> parallelStream1 = productModels.parallelStream();

        IntStream.range(0, 10).forEach(System.out::println);
    }
}

class ProductData {
    private int id;
    private String name;

    public ProductData(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ProductData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

class ProductModel {

    private static final Random RANDOM = new Random();
    private int id;
    private String name;
    private boolean inStock;
    private int stock = RANDOM.nextInt(100);

    public ProductModel(int id, String name, boolean inStock) {
        this.id = id;
        this.name = name;
        this.inStock = inStock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }

    @Override
    public String toString() {
        return "ProductModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", inStock=" + inStock +
                ", stock=" + stock +
                '}';
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
