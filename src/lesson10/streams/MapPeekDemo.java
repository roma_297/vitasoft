package lesson10.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MapPeekDemo {
    private static final List<ProductModel> PRODUCT_MODELS = Arrays.asList(
            new ProductModel(32, "Мыло", true),
            new ProductModel(0, "Гель", true),
            new ProductModel(2, "Мочалка", true),
            new ProductModel(5, "Веревка", false),
            new ProductModel(4, "Шампунь", true),
            new ProductModel(7, "Табуретка", true),
            new ProductModel(11, "Петля", false)
    );

    public static void main(String[] args) {
        Integer stock = PRODUCT_MODELS.stream()
                .sorted(Comparator.comparingInt(ProductModel::getStock))
                .peek(System.out::println)
                .sorted(Comparator.comparingInt(ProductModel::getId))
                .peek(System.out::println)
                .mapToInt(ProductModel::getStock)
                .reduce((o1, o2) -> o1 + o2).orElse(0);

        System.out.println(stock);


    }
}