package lesson10.streams;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.stream.IntStream;

public class ForEachDemo {
    public static void main(String[] args) {
//        IntStream.range(0, 1000).parallel()
//                .forEachOrdered(System.out::println);

//        int[] ints = IntStream.iterate(0, i -> i + 2)
//                .limit(50)
//                .toArray();

//        System.out.println(Arrays.toString(ints));

        BigDecimal factorial = IntStream.range(1, 6)
                .mapToObj(BigDecimal::valueOf)
                .reduce(BigDecimal::multiply).orElse(new BigDecimal(0));

        System.out.println(factorial);
    }
}
