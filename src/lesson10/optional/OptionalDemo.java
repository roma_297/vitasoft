package lesson10.optional;

import java.util.Optional;
import java.util.Random;

public class OptionalDemo {

    private static Random RANDOM = new Random();

    public static void main(String[] args) {




        Optional.of(null);
        Optional<String> optional = Optional.ofNullable(generateString());

//        System.out.println(optional.orElseGet(() -> {
////
////            Some logic
//            return "?????";
//        }));

        optional.ifPresent(value -> {
            System.out.println(value);
        });

        optional.orElseThrow(RuntimeException::new);
    }

    private static String generateString() {
        switch (RANDOM.nextInt(3)) {
            case 1:
                System.out.println("Empty string generated");
                return "";
            case 2:
                String s = String.valueOf(RANDOM.nextInt(1000));
                System.out.println("String " + s + " generated");
                return s;
            default:
                System.out.println("null generated");
                return null;
        }
    }
}
