package lesson8.bitSet;

import java.util.BitSet;

public class BitSetDemo {
    public static void main(String[] args) {
        BitSet bitSet1 = new BitSet();
        bitSet1.set(0, 4);
        bitSet1.set(2, false);

        System.out.println(bitSet1);
    }
}
