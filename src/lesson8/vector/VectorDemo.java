package lesson8.vector;

import java.util.Arrays;
import java.util.Vector;

public class VectorDemo {
    public static void main(String[] args) {
        Vector<String> vector = new Vector<>(3, 2);
        System.out.println(vector.capacity());
        System.out.println(vector.size());

        vector.addAll(Arrays.asList("", "", "", ""));

        System.out.println(vector.capacity());

//        vector.add("str1");
//        vector.addAll(Arrays.asList("str2", "str3"));
//
//        vector.forEach(System.out::println);
//
//        System.out.println("Vector contains 'str2': " + vector.contains("str2"));
//
//        vector.remove(1);
//
//        System.out.println("Vector contains 'str2': " + vector.contains("str2"));
    }
}
