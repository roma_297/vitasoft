package lesson8.set;

import java.util.HashSet;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

public class SetDemo {
    public static void main(String[] args) {
        Set<Person> persons = new HashSet<>();

        persons.add(new Person());
        persons.add(new Person());
        persons.add(new Person());
        persons.add(new Person());

        persons.forEach(person -> System.out.println(person + " " + person.hashCode()));
    }
}

class Person {
    String name;
    String age;
    boolean age1;
    int age2;
    String age3;
    String age4;
    Integer age5;

    @Override
    public int hashCode() {
        return Objects.hash(name, age, age1, age2, age3, age4, age5);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name);
    }
}

