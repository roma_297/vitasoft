package lesson8.stack;

import java.util.Stack;

public class StackDemo {
    public static void main(String[] args) {
        Stack<String> strings = new Stack<>();

        System.out.println("Stack is empty: " + strings.empty());

        strings.push("str1");
        strings.push("str2");
        strings.push("str3");

        strings.forEach(System.out::println);

        System.out.println("After pop: ");

        System.out.println(strings.search("str1"));

        strings.forEach(System.out::println);
    }
}
