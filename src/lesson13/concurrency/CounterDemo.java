package lesson13.concurrency;

public class CounterDemo {
    public static void main(String[] args) {
//        Thread thread1 = new Thread(new CounterThread());
//        Thread thread2 = new Thread(new CounterThread());
//        thread1.start();
//        thread2.start();
//
//        try {
//            thread1.join();
//            thread2.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println(CounterThread.getCount());

        Counter counter = new Counter();
        Thread thread3 = new Thread(new IncreaseCounterThread(counter));
        Thread thread4 = new Thread(new IncreaseCounterThread(counter));
        thread3.start();
        thread4.start();

        try {
            thread3.join();
            thread4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(counter.getCount());
    }
}

class CounterThread implements Runnable {

    private static int count = 0;
    private static final Object LOCK = new Object();

    public static int getCount() {
        return count;
    }

    @Override
    public void run() {
        for (int i = 0; i < 50_000; i++) {
            synchronized (LOCK) {
                count++;
            }
        }
    }
}

class IncreaseCounterThread implements Runnable {
    private Counter counter;

    public IncreaseCounterThread(Counter counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        for (int i = 0; i < 50_000; i++) {
            counter.incrementCounter();
        }
    }
}

class Counter {
    private int count = 0;

    synchronized public void incrementCounter() {
        count++;
    }

    public int getCount() {
        return count;
    }
}