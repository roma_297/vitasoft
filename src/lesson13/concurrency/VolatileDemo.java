package lesson13.concurrency;

public class VolatileDemo {
    public static void main(String[] args) {
        Clicker clicker = new Clicker();
        clicker.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clicker.stopClick();
    }
}

class Clicker extends Thread {
    int click = 0;
    private volatile boolean running = true;
    int count;
 
    public Clicker() {
    }

    public void run() {
        while (running) {
            click++;
        }
        System.out.println(click == count);
    }
 
    public void stopClick() {
        running = false;
        count = click;
    }
}
