package lesson13.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WaitNotifyDemo {
    public static void main(String[] args) {
        SharedResource sharedResource = new SharedResource();

        new IntegerSetterGetter("thread1", sharedResource).start();
        new IntegerSetterGetter("thread2", sharedResource).start();
        new IntegerSetterGetter("thread3", sharedResource).start();
        new IntegerSetterGetter("thread4", sharedResource).start();
        new IntegerSetterGetter("thread5", sharedResource).start();
    }
}

class SharedResource {
    private List<Integer> list;


    public SharedResource() {
        list = new ArrayList<Integer>();
    }


    public void setElement(Integer element) {
        list.add(element);
    }


    public Integer getELement() {
        if (list.size() > 0) {
            return list.remove(0);
        }
        return null;
    }
}

class IntegerSetterGetter extends Thread {
    private SharedResource resource;
    private boolean run;

    private Random rand = new Random();

    public IntegerSetterGetter(String name, SharedResource resource) {
        super(name);
        this.resource = resource;
        run = true;
    }

    public void stopThread() {
        run = false;
    }

    public void run() {
        int action;

        try {
            while (run) {
                action = rand.nextInt(1000);
                if (action % 2 == 0) {
                    getIntegersFromResource();
                } else {
                    setIntegersIntoResource();
                }
            }
            System.out.println("Поток " + getName()
                    + " завершил работу.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void getIntegersFromResource()
            throws InterruptedException {
        Integer number;

        synchronized (resource) {
            System.out.println("Поток " + getName()
                    + " хочет извлечь число.");
            number = resource.getELement();
            while (number == null) {
                System.out.println("Поток " + getName()
                        + " ждет пока очередь заполнится.");
                resource.wait();
                System.out.println("Поток " + getName()
                        + " возобновил работу.");
                number = resource.getELement();
            }
            System.out.println("Поток " + getName() + " извлек число " + number);
        }
    }

    private void setIntegersIntoResource()
            throws InterruptedException {
        Integer number = rand.nextInt(500);
        synchronized (resource) {
            resource.setElement(number);
            System.out.println("Поток " + getName() + " записал число " + number);
            resource.notify();
        }
    }
}



