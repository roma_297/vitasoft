package lesson1;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        BigDecimal bigDecimal1 = new BigDecimal(1000);
        BigDecimal bigDecimal2 = new BigDecimal(3200);

        BigDecimal sum = bigDecimal1.add(bigDecimal2).multiply(bigDecimal1).divide(bigDecimal1);
        System.out.println(sum);
    }
}



