package lesson3;

import java.util.Arrays;
import java.util.List;

public class Main extends Object {
    public static void main(String[] args) {
        createPersons().forEach(System.out::println);
    }

    private static List<Person> createPersons() {
        return Arrays.asList(new Person("John"), new Person("Anna"), new Person("Paul"));
    }
}

class Person {
    private String name;
    private int age;

    public Person(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
