package lesson4.package1;

import java.io.IOError;
import java.util.ArrayList;

public class Client {
    String firstName = this.toString();
    private String lastName;
    private int age;
    private boolean isMale;

    public static void main(String[] args) {
        float f = 123_12.3F;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    @Override
    public String toString() {
        return "Client{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isMale=" + isMale +
                '}';
    }
}

class A {
    int x = 10;
}

class B extends A {
    public static void main(String[] args) {
        ArrayList<String> objects = new ArrayList<>();
        objects.add("a");
        objects.add("b");
        objects.add(1, "c");

        System.out.println(objects);
        System.out.println(objects.subList(1, 1));
    }
    public void get() throws IOError {

    }
}
//
//interface Int {
//Integer
//}


