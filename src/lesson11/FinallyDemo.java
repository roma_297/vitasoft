package lesson11;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.DataFormatException;

public class FinallyDemo {
    private static final Logger LOG = Logger.getLogger(FinallyDemo.class.getName());

    public static void main(String[] args) {

        try {
            new ProductDAO().someDangerousMethod();
        } catch (DataFormatException | ArithmeticException | UnsupportedOperationException  e) {
            LOG.log(Level.ALL, "message");
            addFlashMessages("message");
        }

//        System.out.println(method());
    }

    private static void addFlashMessages(String message) {
    }

    private static int method() {
        try {
            return 10;
        } finally {
            return 20;
        }
    }
}

class ProductDAO implements Cloneable {
    private static final List<Product> PRODUCTS = Arrays.asList(new Product(), new Product(), new Product());

    public List<Product> getAllProducts() {
        Random random = new Random();

        System.out.println("Open DB connection");

        List<Product> products = null;
        try {
            if (random.nextInt(2) == 0) {
                products = PRODUCTS;
            } else {
                throw new RuntimeException();
            }
        } catch (RuntimeException e) {
            System.out.println("Exception was caught: " + Arrays.toString(e.getStackTrace()));
        } finally {
            System.out.println("Close DB connection");
        }

        return products;
    }

    public int someDangerousMethod() throws DataFormatException, UnsupportedOperationException, ArithmeticException {
        Random random = new Random(3);

        switch (random.nextInt()) {
            case 0:
                throw new UnsupportedOperationException();
            case 1:
                throw new DataFormatException();
            case 2:
                throw new ArithmeticException();
            default:
                break;
        }

        return 0;
    }
}

class Product {
}
