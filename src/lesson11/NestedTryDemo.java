package lesson11;

import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;

public class NestedTryDemo {
    public static void main(String[] args) {

//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            String number = bufferedReader.readLine();
//            int string = Integer.parseInt(number);
//
//            System.out.println(string);
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                bufferedReader.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void remoteMethod() throws RemoteException{
        try{
            throw new RemoteException("this is RemoteException");
        } catch(Exception e){
            throw e;
        }
    }

}
