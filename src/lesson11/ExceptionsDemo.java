package lesson11;

import java.util.Arrays;
import java.util.Random;

public class ExceptionsDemo {

    private static final double EPS = 1e-6;

    public static void main(String[] args) {
        Random random = new Random();
        int a = random.nextInt(10);
        int b = random.nextInt(2);

        System.out.println("a=" + a + "; b=" + b);

        try {
            System.out.println(divide(a, b));
        } catch (Exception e) {
            System.out.println(e.getMessage() + ": " + Arrays.toString(e.getStackTrace()));
        }
    }

    static double divide(double a, double b) throws UnsupportedOperationException {
        if (Math.abs(b) < EPS) {
            throw new UnsupportedOperationException("Value of b mustn't be equal zero");
        }

        return a / b;
    }
}
